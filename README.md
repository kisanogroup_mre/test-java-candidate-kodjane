# README #

This is a stub for an application that takes two parameters: **firstName** & **filePath**.

The application starts an embedded webserver that serves two different paths: **/discovery** & **/uuid**

The webserver is started on a random port in a range **[2375 ; 2380]**. This means that valid URLs to access the paths are:

*  **http://localhost:{port}/discovery?name={firstName}**  
When it is called (HTTP GET), it will return the 200 HTTP code and the following string (mimeType: TEXT_PLAIN): <ins>"Hi {firstName}, I'm KISANO server"</ins>.  
Using this method, you can verify that you're in fact talking to the KISANO server.
*  **http://localhost:{port}/uuid**
When it is called (HTTP GET), it will return the 200 HTTP code and a random UUID.

## Your goals are ##

*  to implement a method to find the port on which the KISANO server is listening
*  to implement a thread named "UUIDGeneratorThread" inheriting the KisanoThread.  
This thread should call the /uuid path (HTTP GET) every 10 seconds to get a new UUID.  
This UUID should be appended as new line  with the following format: **"{now}[YYYY-MM-dd HH:mm:ss]: {uuid}"** to a file located at {filePath} (created if necessary)

You may NOT modify HttpServer.java 
Ports retrieval and communication must happen via HTTP calls, not java calls to HttpServer directly.

## Requirements ##

*  Java 11
*  Maven